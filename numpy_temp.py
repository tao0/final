import openpyxl
import sqlite3
import numpy as np
import matplotlib.pyplot as plt

wb = openpyxl.load_workbook("World Temperture.xlsx")
sheet = wb.create_sheet(index=1)
sheet.title = 'Comparison'
ws = wb.get_sheet_by_name("Comparison")


connection = sqlite3.connect("GlobalLandTemperatures.db")
cursor = connection.cursor()

states = ['New South Wales','Queensland','South Australia','Victoria','Western Australia','Northern Territory','Tasmania','Australian Capital Territory']
i = 1
ws['A1']= 'Year'
for state in states:
    i += 1
    ws.cell(row=1,column=i).value = state

TempsofAus = []
years = []
for y in range(1855, 2012):
    sql_command = """
        SELECT AVG(averageTemp)
        FROM GlobalLandTemperaturesByCountry
        WHERE timea LIKE '%{0}%' and country = 'Australia'""".format(y)
    cursor.execute(sql_command)
    TempsyearofAus = cursor.fetchall()
    str0 = str(TempsyearofAus)
    TempsofAus.append(float(str0[2:-3]))
    years.append(y)

list_NSW = []
list_QLD = []
list_SA = []
list_VIC = []
list_WA = []
list_TAS = []
list_NT = []
list_ACT = []
for state in states:
    #TempsofState = []
    for y in range(1855, 2012):
        sql_command = """
            SELECT AVG(averageTemp)
            FROM GlobalLandTemperaturesByState
            WHERE timea LIKE '%{0}%' and state = '{1}' and country = 'Australia'""".format(y,state)
        cursor.execute(sql_command)
        TempsyearofState = cursor.fetchall()
        str1 = str(TempsyearofState)
        str2 = str1[2:-3]
        #TempsofState.append(float(str1[2:-3]))
        if str1 == "" or str2 == "None":
            pass
        else:
            if state == 'New South Wales':
                list_NSW.append(float(str1[2:-3]))
            elif state == 'Queensland':
                list_QLD.append(float(str1[2:-3]))
            elif state == 'South Australia':
                list_SA.append(float(str1[2:-3]))
            elif state == 'Victoria':
                list_VIC.append(float(str1[2:-3]))
            elif state == 'Western Australia':
                list_WA.append(float(str1[2:-3]))
            elif state == 'Northern Territory':
                list_NT.append(float(str1[2:-3]))
            elif state == 'Tasmania':
                list_TAS.append(float(str1[2:-3]))
            elif state == 'Australian Capital Territory':
                list_ACT.append(float(str1[2:-3]))

na = np.array(TempsofAus)
a = np.array(list_NSW)
b = np.array(list_QLD)
c = np.array(list_SA)
d = np.array(list_VIC)
e = np.array(list_WA)
f = np.array(list_NT)
g = np.array(list_TAS)
h = np.array(list_ACT)

Da = na - a
Db = na - b
Dc = na - c
Dd = na - d
De = na - e
Df = na - f
Dg = na - g
Dh = na - h


plt.figure(1)
plt.subplot(811)
plt.plot(years,a,"b--")
plt.xlabel('year')
plt.ylabel('NSW')
plt.title('The differences of tempurture between each state and the national data for each year')

plt.subplot(812)
plt.plot(years,b,"b--")
plt.xlabel('year')
plt.ylabel('QLD')

plt.subplot(813)
plt.plot(years,c,"b--")
plt.xlabel('year')
plt.ylabel('SA')

plt.subplot(814)
plt.plot(years,d,"b--")
plt.xlabel('year')
plt.ylabel('VIC')

plt.subplot(815)
plt.plot(years,e,"b--")
plt.xlabel('year')
plt.ylabel('WA')

plt.subplot(816)
plt.plot(years,f,"b--")
plt.xlabel('year')
plt.ylabel('NT')

plt.subplot(817)
plt.plot(years,g,"b--")
plt.xlabel('year')
plt.ylabel('TAS')

plt.subplot(818)
plt.plot(years,h,"b--")
plt.xlabel('year')
plt.ylabel('ACT')
plt.show()

rss = 0
for num in range(1855,2012):
    rss += 1
    print(rss,num)
    ws.cell(row=rss+1, column=1).value = num
    ws.cell(row=rss+1, column=2).value = Da[rss-1]
    ws.cell(row=rss+1, column=3).value = Db[rss-1]
    ws.cell(row=rss+1, column=4).value = Dc[rss-1]
    ws.cell(row=rss+1, column=5).value = Dd[rss-1]
    ws.cell(row=rss+1, column=6).value = Df[rss-1]
    ws.cell(row=rss+1, column=8).value = Dg[rss-1]
    ws.cell(row=rss+1, column=9).value = Dh[rss-1]

wb.save('World Temperture.xlsx')
wb.close()