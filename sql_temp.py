import sqlite3

connection = sqlite3.connect("GlobalLandTemperatures.db")
cursor = connection.cursor()

sql_command = """
CREATE TABLE Southerncities
(city CHAR(20),
country CHAR(20),
latitude CHAR(10),
longitude CHAR(10))
"""
cursor.execute(sql_command)
print("sql_command done")

sql_command1 = """
SELECT DISTINCT *
FROM GlobalLandTemperaturesByMajorCity
WHERE latitude LIKE '%S'
ORDER BY country;
"""
cursor.execute(sql_command1)
list = cursor.fetchall()
print("sql_command1 done")

for i in list:
    a = i[0]
    b = i[1]
    c = i[2]
    d = i[3]
    sql_command2 ="""
    INSERT INTO Southerncities
    VALUES ('{a}', '{b}', '{c}', '{d}')"""
    cursor.execute(sql_command2)

sql_command3 = """
SELECT MAX(averageTemp), MIN(averageTemp), AVG(averageTemp)
FROM GlobalLandTemperaturesByState
WHERE timea LIKE '%2010%' and state = 'Queensland';
"""
cursor.execute(sql_command3)
print(cursor.fetchall())
cursor.close()
connection.commit()
connection.close()