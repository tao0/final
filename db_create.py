import openpyxl
import sqlite3


wb1 = openpyxl.load_workbook("GlobalLandTemperaturesByCountry.xlsx")
ws1 = wb1.get_sheet_by_name("GlobalLandTemperaturesByCountry")
wb2 = openpyxl.load_workbook("GlobalLandTemperaturesByMajorCity.xlsx")
ws2 = wb2.get_sheet_by_name("GlobalLandTemperaturesByMajorCi")
wb3 = openpyxl.load_workbook("GlobalLandTemperaturesByState.xlsx")
ws3 = wb3.get_sheet_by_name("GlobalLandTemperaturesByState")
print(ws1.title,ws2.title,ws3.title)

connection = sqlite3.connect("GlobalLandTemperatures.db")
cursor = connection.cursor()
sql_command1 = """
CREATE TABLE GlobalLandTemperaturesByCountry
(timea DATE, 
averageTemp FLOAT, 
averageTempUn FLOAT, 
country CHAR(20),
PRIMARY KEY(timea,country))"""
cursor.execute(sql_command1)
print "sql_command1 done"

sql_command2 = """
CREATE TABLE GlobalLandTemperaturesByMajorCity
(timea DATE,
averageTemp FLOAT,
averageTempUn FLOAT, 
city CHAR(20),
country CHAR(20),
latitude CHAR(10),
longitude CHAR(10),
PRIMARY KEY(timea,city))"""
cursor.execute(sql_command2)
print "sql_command2 done"

sql_command3 = """
CREATE TABLE GlobalLandTemperaturesByState
(timea DATE,
averageTemp FLOAT,
averageTempUn FLOAT,
state CHAR(20), 
country CHAR(20),
PRIMARY KEY(timea,state))"""
cursor.execute(sql_command3)
print "sql_command3 done"

max_rows1 = ws1.max_row
for r in range(2, max_rows1):
    timea = ws1.cell(row=r, column=1).value
    averageTemp = ws1.cell(row=r, column=2).value
    averageTempUn = ws1.cell(row=r, column=3).value
    country = ws1.cell(row=r, column=4).value
    sql_command4 = """
    INSERT INTO GlobalLandTemperaturesByCountry(timea,averageTemp,averageTempUn,country)
    VALUES('""" +str(timea)+ "','"+ str(averageTemp)+"','"+ str(averageTempUn)+"',\"" + country+ "\")"
    print(sql_command4)
    cursor.execute(sql_command4)
print("sql_command4 done")


max_rows2 = ws2.max_row
for r in range(2, max_rows2):
    data2 = ws2.cell(row=r, column=1).value
    averageTemp2 = ws2.cell(row=r, column=2).value
    averageTempUn2 = ws2.cell(row=r, column=3).value
    city2 = ws2.cell(row=r, column=4).value
    country2 = ws2.cell(row=r, column=5).value
    latitude = ws2.cell(row=r, column=6).value
    longitude = ws2.cell(row=r, column=7).value
    sql_command5 = """
    INSERT INTO GlobalLandTemperaturesByMajorCity(timea, averageTemp, averageTempUn, city, country, latitude, longitude)
    VALUES('"""+str(data2)+"','" + str(averageTemp2)+"','" + str(averageTempUn2)+ "','" + city2+ "',\"" + country2+ "\",'"+ latitude+ "','"+ longitude+ "')"
    cursor.execute(sql_command5)
print("sql_command5 done")

max_rows3 = ws3.max_row
for r in range(2, max_rows3):
    data3 = ws3.cell(row=r, column=1).value
    averageTemp3 = ws3.cell(row=r, column=2).value
    averageTempUn3 = ws3.cell(row=r, column=3).value
    state = ws3.cell(row=r, column=4).value
    country3 = ws3.cell(row=r, column=5).value
    sql_command6 = """
    INSERT INTO GlobalLandTemperaturesByState(timea,averageTemp,averageTempUn,state,country)
    VALUES('"""+str(data3)+ "','"+ str(averageTemp3)+"','"+ str(averageTempUn3)+"',\"" +state+"\",\"" + country3+ "\")"
    cursor.execute(sql_command6)
print("sql_command6 done")

cursor.close()
connection.commit()
connection.close()
print "Done"