import sqlite3
import openpyxl

wb = openpyxl.load_workbook("World Temperture.xlsx")
sheet = wb.create_sheet(index=0)
sheet.title = 'TemperatureByCity'
ws = wb.get_sheet_by_name("TemperatureByCity")
ws['A1'] = 'Year'
ws['B1'] = 'AverageTemperture'
ws['C1'] = 'City'
ws['D1'] = 'Country'

connection = sqlite3.connect("GlobalLandTemperatures.db")
cursor = connection.cursor()

country = 'China'



sql_command = """
SELECT timea
FROM GlobalLandTemperaturesByMajorCity
WHERE country = 'China'"""

cursor.execute(sql_command)
list_y = cursor.fetchall()
ys = []
for i in list_y:
    i = str(i)
    ys.append(int(i[3:7]))
print(len(ys))


sql_command1 = """
SELECT city
FROM GlobalLandTemperaturesByMajorCity
WHERE country = 'China'"""

cursor.execute(sql_command1)
list_c = cursor.fetchall()
citys = []
for i in list_c:
    i = str(i)
    citys.append(i[3:-3])
print(len(citys))


index = 0
rs = 2
for year in ys:
    yy = int(year)
    if(index < len(ys)-1):
            index += 1
    else:
        break

    if ys[index-1] != ys[index] and citys[index-1] == citys[index]:
        a = ys[index-1]
        b = citys[index-1]
        print(index)
        print(a,b)
        sql_command3 = """
            SELECT timea,averageTemp,city,country
            FROM GlobalLandTemperaturesByMajorCity
            WHERE timea LIKE '%{0}%' and city = '{1}' and country = 'China'""".format(a, b)
        cursor.execute(sql_command3)
        list1 = cursor.fetchall()
        TotalT = 0.0
        avgT = 0.0
        i = 0
        for r in list1:
            if r[1] != "" and r[1] != "None":
                i += 1
                Tem = float(r[1])
                TotalT = TotalT + Tem
        if i > 0:
            avgT = TotalT / i
        list2 = [a, avgT, b]
        print(list2[0],list2[1],list2[2])

        ws.cell(row=rs, column=1).value = list2[0]
        ws.cell(row=rs, column=2).value = list2[1]
        ws.cell(row=rs, column=3).value = list2[2]
        ws.cell(row=rs, column=4).value = 'China'
        rs += 1
    else:
        pass

wb.save('World Temperture.xlsx')
wb.close()